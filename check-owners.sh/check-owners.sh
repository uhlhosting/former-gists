#!/bin/sh
# filename: check-owners.sh
# For https://forums.fedoraforum.org/showthread.php?319045-Detecting-File-Permission-and-Ownership-Changes&p=1811694#post1811694

# PARSE INPUT
CO_INPUT="$( readlink -f "${1:-.}" 2>/dev/null )"                   # read first parameter given, or use $PWD
CO_TMPFILE="$( mktemp )"
test -z "${CO_OUTPUT}" && \
   CO_OUTPUT="/var/cache/check-owners/co${CO_INPUT//\//.}.db.gz"    # use directory-specific database file
test ! -e "$( dirname "${CO_OUTPUT}" )" && \
   mkdir -p "$( dirname "${CO_OUTPUT}" )"                           # make the cache directory for the databases

# DEBUG
test -n "${CO_DEBUG}" && echo "Checking directory ${CO_INPUT}" 1>&2 # set CO_DEBUG to any value to get debugging info on stderr

# FUNCTIONS
scan_dir() {
   # call: scan_dir "${CO_INPUT}"
   # output: listing of hash, owner+perm hash for each file
   local td="${1}"

   find "${td}" -exec stat -L -c '%u,%U,%g,%G,%a,%n' {} + 2>/dev/null | sort -t ',' -k6
}

clean_co() {
   # remote tempfiles
   rm -f "${CO_TMPFILE}"
}

# TRAPS

# ignore broken pipes, because we still want to update the database file
trap '' 13 # SIGPIPE

# regular traps
trap "__ec=$? ; clean_co ; trap '' 0 1 2 3 4 5 6 7 8 9 10 11 12 14 15 16 17 18 19 20 ; exit ${__ec} ;" 0 1 2 3 4 5 6 7 8 9 10 11 12 14 15 16 17 18 19 20  # clean up when exiting


echo "DEBUG INFO:"
ls -l "/proc/$$/fd/1"

# MAIN
if test -z "$( zcat "${CO_OUTPUT}" 2>/dev/null | head -n3 )" ;
then

   # database is empty so write it the first time
   test -n "${CO_DEBUG}" && echo "Initializing database \"${CO_OUTPUT}\" during this run..." 1>&2
   scan_dir "${CO_INPUT}" | tee "/proc/$$/fd/1" | gzip > "${CO_OUTPUT}"

else

   # not empty
   test -n "${CO_DEBUG}" && echo "Comparing ${CO_INPUT} to database ${CO_OUTPUT}"

   # learn current status
   scan_dir "${CO_INPUT}" > "${CO_TMPFILE}"

   # compare to database
   zcat "${CO_OUTPUT}" | diff -W300 --suppress-common-lines -y "-" "${CO_TMPFILE}"

   # replace database
   cat "${CO_TMPFILE}" | gzip > "${CO_OUTPUT}"

fi

# EXIT
true
