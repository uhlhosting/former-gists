#!/bin/sh
# Filename: find_expiring_certs.sh
# Location: 
# Author: bgstack15@gmail.com
# Startdate: 2018-07-16 10:26:52
# Title: Script that Reports on Local Certs That Will Expire Soon
# Purpose: To find certs that will expire soon
# Package: 
# History: 
# Usage: 
# Reference: ftemplate.sh 2018-05-15m; framework.sh 2017-11-11m
# Improve:
# Documentation:
#    This tool searches for ssl certificate files and reports the ones that will expire soon. The find mechanism does not operate very well, but it does work.
fiversion="2018-05-15m"
find_expiring_certsversion="2018-07-16a"

usage() {
   ${PAGER:-/usr/bin/less -F} >&2 <<ENDUSAGE
usage: find_expiring_certs.sh [-duV] [-c conffile]
version ${find_expiring_certsversion}
 -d debug   Show debugging info, including parsed variables.
 -u usage   Show this usage block.
 -V version Show script version number.
 -c conf    Read in this config file.
Return values:
 0 Normal
 1 Help or version info displayed
 2 Count or type of flaglessvals is incorrect
 3 Incorrect OS type
 4 Unable to find dependency
 5 Not run as root or sudo
Environment variables:
FEC_FIND_METHOD locate|find|stdin. Locate is default. Stdin reads one file name per line.
FEC_EXCLUDE_DIRS  Colon-separated list of directories. default: /usr/share/doc
FEC_FILE_FILTER   '*.pem|*.crt'   Filename globbing to restrict matches
FEC_DAYS 30  Number of days out to warn about
FEC_DATE     Absolute number. Derived form FEC_DAYS, or you can specify it.
FEC_OPENSSL  Alternate location to openssl executable
FEC_DATE_COMMAND  Alternate location to date executable
ENDUSAGE
}

# DEFINE FUNCTIONS
find_all_certs() {
   # call: find_all_certs "${FEC_FIND_METHOD}" "${FEC_EXCLUDE_DIRS}" "${FEC_FILE_FILTER}"
   # output: one file per line of stdout
   debuglev 8 && ferror "find_all_certs $@"
   local find_method="${1}"
   local exclude_dirs="${2}"
   local file_filter="${3}"

   # Find all cert files
   case "${find_method:-locate}" in

      locate)
         # split exclude_dirs
         echo "${exclude_dirs}" | tr ':' '\n' > "${FEC_tmpfile2}"
         # split file_filter
         {
            echo "${file_filter}" | tr '|' '\n' | while read tpattern ;
            do
               locate "${tpattern}"
            done
         } | grep -v -f "${FEC_tmpfile2}"
         ;;

      stdin)
         # means read in files or paths from standard input
         cat > "${FEC_tmpfile1}"
         cat "${FEC_tmpfile1}"
         ;;

      find)
         # need to split FEC_FILE_FILTER
         # GOOD
         local find_filter="$( x=0 ; printf '\( ' ; echo "${file_filter}" | tr '|' '\n' | while read tfilter ; do x=$((x+1)) ; if test $x -ge 2 ; then printf -- '-o ' ; fi ; printf -- "-name %s " "${tfilter}" ; done ; printf '\) \n' )"
         # need to split FEC_EXCLUDE_DIRS
         local prune_dirs=""
         test -n "${exclude_dirs}" && {
            prune_dirs="$( echo "${exclude_dirs}" | tr ':' '\n' | while read td ; do printf -- '-path %s -prune ' "${td}" ; done ; printf '\n' )"
         }
         echo /bin/find / ${prune_dirs} ${find_filter} 1>&2
         eval /bin/find / ${prune_dirs} ${find_filter} 
         ;;

   esac
}

limit_certs() {
   # call: limit_certs "${FEC_DATE}" "${VERBOSE}"
   # read files from standard input and print out filenames of certs that will expire within the threshold
   debuglev 8 && ferror "limit_certs $@"
   local expiration_date="${1}"

   local this_exp_date=0
   local this_expired=0

   while read this_file ;
   do
      this_expired=0
      this_exp_date="$( "${FEC_OPENSSL:-/bin/openssl}" x509 -in "${this_file}" -noout -enddate 2>/dev/null | sed -r -e 's/notAfter=//;' )"
      if test -n "${this_exp_date}" ;
      then
         this_exp_epoch="$( "${FEC_DATE_COMMAND:-/bin/date}" --date "${this_exp_date}" "+%s" 2>/dev/null )"
      else
         this_exp_epoch="$(( expiration_date + 604800 ))"
      fi
      
      if test ${this_exp_epoch} -le ${expiration_date} ;
      then
         this_expired=1
         printf "%s\n" "${this_file}"
      fi
      #printf "%-80s  %s  %s %s\n" "${this_file}" "${this_expired}" "${this_exp_epoch}" "${this_exp_date}"
   done

}

display_certs() {
   # call: display_certs "${dangerous_certs}" "${verbose}"
   # output: show tabular information about the certs, listed one per line in dangerous_certs
   debuglev 8 && ferror "display_certs $@"
   local these_certs="${1}"
   local verbose="${2}" # v=0 just filename, v=1 show filename and date, v=2 show additional properties

   local this_exp_date=0
   local this_all_output=""
   local this_serial=""

   echo "${these_certs}" | while read this_file ;
   do
      this_all_output="$( "${FEC_OPENSSL:-/bin/openssl}" x509 -in "${this_file}" -noout -subject -issuer -dates -serial -fingerprint 2>/dev/null )"
      #echo "${this_all_output}"
      this_exp_date="$( echo "${this_all_output}" | sed -n -e '/notAfter/{s/notAfter=//;p;}' )"
      this_serial="$( echo "${this_all_output}" | sed -n -e '/serial=/{s/serial=//;p;}' )"
      printf "%-80s  %s  %s %s\n" "${this_file}" "${this_serial}" "${this_exp_date}"
      
   done
}

# DEFINE TRAPS

clean_find_expiring_certs() {
   # use at end of entire script if you need to clean up tmpfiles
   # rm -f "${tmpfile1}" "${tmpfile2}" 2>/dev/null

   # Delayed cleanup
   if test -z "${FETCH_NO_CLEAN}" ;
   then
      nohup /bin/bash <<EOF 1>/dev/null 2>&1 &
sleep "${find_expiring_certs_CLEANUP_SEC:-2}" ; /bin/rm -r "${FEC_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

CTRLC() {
   # use with: trap "CTRLC" 2
   # useful for controlling the ctrl+c keystroke
   :
}

CTRLZ() {
   # use with: trap "CTRLZ" 18
   # useful for controlling the ctrl+z keystroke
   :
}

parseFlag() {
   flag="$1"
   hasval=0
   case ${flag} in
      # INSERT FLAGS HERE
      "d" | "debug" | "DEBUG" | "dd" ) setdebug; ferror "debug level ${debug}";;
      "u" | "usage" | "help" | "h" ) usage; exit 1;;
      "V" | "fcheck" | "version" ) ferror "${scriptfile} version ${find_expiring_certsversion}"; exit 1;;
      #"i" | "infile" | "inputfile" ) getval; infile1=${tempval};;
      "c" | "conf" | "conffile" | "config" ) getval; conffile="${tempval}";;
   esac
   
   debuglev 10 && { test ${hasval} -eq 1 && ferror "flag: ${flag} = ${tempval}" || ferror "flag: ${flag}"; }
}

# DETERMINE LOCATION OF FRAMEWORK
f_needed=20171111
while read flocation ; do if test -e ${flocation} ; then __thisfver="$( sh ${flocation} --fcheck 2>/dev/null )" ; if test ${__thisfver} -ge ${f_needed} ; then frameworkscript="${flocation}" ; break; else printf "Obsolete: %s %s\n" "${flocation}" "${__this_fver}" 1>&2 ; fi ; fi ; done <<EOFLOCATIONS
./framework.sh
${scriptdir}/framework.sh
$HOME/bin/bgscripts/framework.sh
$HOME/bin/framework.sh
$HOME/bgscripts/framework.sh
$HOME/framework.sh
/usr/local/bin/bgscripts/framework.sh
/usr/local/bin/framework.sh
/usr/bin/bgscripts/framework.sh
/usr/bin/framework.sh
/bin/bgscripts/framework.sh
/usr/local/share/bgscripts/framework.sh
/usr/share/bgscripts/framework.sh
EOFLOCATIONS
test -z "${frameworkscript}" && echo "$0: framework not found. Aborted." 1>&2 && exit 4

# INITIALIZE VARIABLES
# variables set in framework:
# today server thistty scriptdir scriptfile scripttrim
# is_cronjob stdin_piped stdout_piped stderr_piped sendsh sendopts
. ${frameworkscript} || echo "$0: framework did not run properly. Continuing..." 1>&2
infile1=
outfile1=
logfile=${scriptdir}/${scripttrim}.${today}.out
define_if_new interestedparties "bgstack15@gmail.com"
# SIMPLECONF
define_if_new default_conffile "/etc/find_expiring_certs/find_expiring_certs.conf"
define_if_new defuser_conffile ~/.config/find_expiring_certs/find_expiring_certs.conf
test -z "${FEC_TMPDIR}" && FEC_TMPDIR="$( mktemp -d )"
test -z "${FEC_FIND_METHOD}" && FEC_FIND_METHOD="locate"
test -z "${FEC_EXCLUDE_DIRS}" && FEC_EXCLUDE_DIRS="/usr/share/doc"
test -z "${FEC_DAYS}" && test -z "${FEC_DATE}" && FEC_DAYS=30
test -z "${FEC_FILE_FILTER}" && FEC_FILE_FILTER='*.pem|*.crt'
# FEC_DATE will be derived from FEC_DAYS later
FEC_tmpfile1="$( TMPDIR="${FEC_TMPDIR}" mktemp )" # stores stdin
FEC_tmpfile2="$( TMPDIR="${FEC_TMPDIR}" mktemp )" # stores FEC_EXCLUDE_DIRS after splitting

# REACT TO OPERATING SYSTEM TYPE
case $( uname -s ) in
   Linux) : ;;
   FreeBSD) : ;;
   *) echo "${scriptfile}: 3. Indeterminate OS: $( uname -s )" 1>&2 && exit 3;;
esac

## REACT TO ROOT STATUS
#case ${is_root} in
#   1) # proper root
#      : ;;
#   sudo) # sudo to root
#      : ;;
#   "") # not root at all
#      #ferror "${scriptfile}: 5. Please run as root or sudo. Aborted."
#      #exit 5
#      :
#      ;;
#esac

# SET CUSTOM SCRIPT AND VALUES
#setval 1 sendsh sendopts<<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
#/usr/local/share/bgscripts/send.sh -hs  # setvalout maybe be "fail" otherwise
#/usr/share/bgscripts/send.sh -hs        # on success, setvalout="valid-sendsh"
#/usr/local/bin/send.sh -hs
#/usr/bin/mail -s
#EOFSENDSH
#test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. mailer not found. Aborted." && exit 4

# VALIDATE PARAMETERS
# objects before the dash are options, which get filled with the optvals
# to debug flags, use option DEBUG. Variables set in framework: fallopts
validateparams - "$@"

# CONFIRM TOTAL NUMBER OF FLAGLESSVALS IS CORRECT
#if test ${thiscount} -lt 2;
#then
#   ferror "${scriptfile}: 2. Fewer than 2 flaglessvals. Aborted."
#   exit 2
#fi

# LOAD CONFIG FROM SIMPLECONF
# This section follows a simple hierarchy of precedence, with first being used:
#    1. parameters and flags
#    2. environment
#    3. config file
#    4. default user config: ~/.config/script/script.conf
#    5. default config: /etc/script/script.conf
if test -f "${conffile}";
then
   get_conf "${conffile}"
else
   if test "${conffile}" = "${default_conffile}" || test "${conffile}" = "${defuser_conffile}"; then :; else test -n "${conffile}" && ferror "${scriptfile}: Ignoring conf file which is not found: ${conffile}."; fi
fi
test -f "${defuser_conffile}" && get_conf "${defuser_conffile}"
test -f "${default_conffile}" && get_conf "${default_conffile}"

# CONFIGURE VARIABLES AFTER PARAMETERS
# determine FEC_DATE, which is the ultimate checkpoint. If a date has an expiration date at or before this time, it should be reported.
if test -z "${FEC_DATE}" ; # if undefined
then
   if test -z "${FEC_DAYS}" ;
   then
      ferror "${scriptfile}: 2. Invalid FEC_DAYS or FEC_DATE. Aborted."
      exit
   else
      # please convert 
      FEC_DATE="$( "${FEC_DATE_COMMAND:-/bin/date}" --date "now${FEC_DAYS:+${FEC_DAYS} days}" 2>/dev/null )"
   fi
fi

# convert FEC_DATE to "+%s"
FEC_DATE="$( "${FEC_DATE_COMMAND:-/bin/date}" --date "${FEC_DATE}" '+%s' 2>/dev/null )"

## REACT TO BEING A CRONJOB
#if test ${is_cronjob} -eq 1;
#then
#   :
#else
#   :
#fi

# SET TRAPS
#trap "CTRLC" 2
#trap "CTRLZ" 18
trap "__ec=$? ; clean_find_expiring_certs ; trap '' 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 ; exit ${__ec} ;" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

# DEBUG SIMPLECONF
debuglev 5 && {
   ferror "Using values"
   # used values: EX_(OPT1|OPT2|VERBOSE)
   set | grep -iE "^FEC_" 1>&2
}

# MAIN LOOP
#{
   # flow:

   # Find all cert files
   all_certs="$( find_all_certs "${FEC_FIND_METHOD}" "${FEC_EXCLUDE_DIRS}" "${FEC_FILE_FILTER}" )"
   debuglev 7 && { echo "===== ALL CERTS FOUND that match file filter and outside excluded dirs" ; echo "${all_certs}" ; } 1>&2

   # Limit list to those that will expire within the limit
   dangerous_certs="$( echo "${all_certs}" | limit_certs "${FEC_DATE}" )"
   debuglev 6 && { echo "===== CERTS that will expire soon" ; echo "${dangerous_certs}" ; } 1>&2

   # Notify
   printf "%s\n" "---- CERTS -----"
   display_certs "${dangerous_certs}" 0

#} | tee -a ${logfile}

# EMAIL LOGFILE
#${sendsh} ${sendopts} "${server} ${scriptfile} out" ${logfile} ${interestedparties}
